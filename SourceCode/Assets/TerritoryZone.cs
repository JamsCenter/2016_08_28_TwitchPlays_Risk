﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;

public class TerritoryZone : MonoBehaviour {

    [Tooltip("Short reference of the country")]
    [SerializeField]
    private string _nameAlias;

    [Tooltip("The name of the territory")]
    [SerializeField]
    private string _nameOfTheTerritory;


    [Tooltip("Territory that can be accessed by this one")]
    [SerializeField]
    private TerritoryZone [] _nearbyTerritory;


    public string GetTerritoryAlias() { return _nameAlias; }
    public string GetTerritoryName() { return _nameOfTheTerritory; }
    public List<string> GetNearTerritoriesAlias() {
        List<string> resultName = new List<string>();
        for (int i = 0; i < _nearbyTerritory.Length; i++)
        {
            if (_nearbyTerritory[i] == null)
                continue;
            string terriAlias = _nearbyTerritory[i].GetTerritoryAlias();
            if (terriAlias == null)
                continue;
            resultName.Add(terriAlias);
        }
        return resultName;
    }

}
