﻿using UnityEngine;
using System.Collections;

public class AutoHint : MonoBehaviour {

    public CelebrityFinderGame _celebrityGame;
    public int _hintEverySeconds = 10;

    void Start () {

        InvokeRepeating("GiveHint", 0, _hintEverySeconds);
	
	}

    void  GiveHint() {

        _celebrityGame.SetRandomLetterAsFounded();
    }


}
