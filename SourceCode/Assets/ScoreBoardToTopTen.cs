﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ScoreBoardToTopTen : MonoBehaviour {


    public PlayersScoreBoard _playerScoreBoard;

    public Text[] _topPlayersTextDisplay ;

    void Start() {
        _playerScoreBoard._onValueChange.AddListener(RefreshBoard);
        RefreshBoard();
    }

    public void RefreshBoard() {

       List<string> topPlayer = _playerScoreBoard.GetPlayersIdNameSortByScore(_topPlayersTextDisplay.Length);
        for (int i = 0; i < _topPlayersTextDisplay.Length; i++)
        {
            if (i < topPlayer.Count)
            {
                string userName = topPlayer[i];
                int score = _playerScoreBoard.GetPointOf(userName);
                _topPlayersTextDisplay[i].text = userName + " (" + score + ")";
            }
            else _topPlayersTextDisplay[i].text = " - ";
        }


    } 
    
}
