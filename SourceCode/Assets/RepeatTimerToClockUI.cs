using UnityEngine;
using System.Collections;

public class RepeatTimerToClockUI : MonoBehaviour {

    public RepeatCountdownTimer _time;
    public PanelSandTimer _sandTime;
    public PanelDigitalClock _timerDisplay;
	
	void Update () {

        _sandTime.SetTimerTo(_time.GetPourcentLoad());
        _timerDisplay.SetTimeTo(_time.GetTimeLeftInSeconds());


    }
}
