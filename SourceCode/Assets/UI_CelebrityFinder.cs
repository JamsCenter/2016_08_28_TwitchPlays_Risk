﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_CelebrityFinder : MonoBehaviour {

    public CelebrityFinderGame _celebrityFinder;

    public Text _hiddenName;
    public Image _hiddenImage;
    public Image _optiqueHidder;


    public void Refresh() {

        if (_hiddenName)
            _hiddenName.text = _celebrityFinder.GetHiddenName();
        if (_hiddenImage) {
            _hiddenImage.enabled = true;
            _hiddenImage.sprite = _celebrityFinder.GetImageOfCelebrity();

        }
        if (_optiqueHidder)
        _optiqueHidder.fillAmount = 1f-_celebrityFinder.GetPourcentageDiscovered();
    }

}
