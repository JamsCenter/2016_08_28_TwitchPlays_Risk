﻿using UnityEngine;
using System.Collections;

public class UI_Skills : MonoBehaviour {


    public GameObject _uiProgrammer;
    public GameObject _uiGameDesigner;
    public GameObject _uiSoundDesigner;
    public GameObject _uiGraphicArtist;



    public void SetSkillsOn(bool isProgrammer, bool isGamedesigner, bool isSoundDesigner, bool isGraphicAritst) {

        _uiProgrammer.SetActive(isProgrammer);
        _uiProgrammer.SetActive(isGamedesigner);
        _uiProgrammer.SetActive(isSoundDesigner);
        _uiProgrammer.SetActive(isGraphicAritst);
    }
    
}
