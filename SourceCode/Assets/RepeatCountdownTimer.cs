using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

public class RepeatCountdownTimer : MonoBehaviour {

    public float _delayOfTheCountDown = 120f;
    public float _countdownValue = 0;

    public bool _autoStart=true;

    internal float GetPourcentLoad()
    {
        return Mathf.Clamp(1f-(_countdownValue / _delayOfTheCountDown), 0f, 1f);
    }

    public UnityEvent _onTimerStartEvent;
    public UnityEvent _onTimerEndEvent;

    public delegate void OnTimerChange(RepeatCountdownTimer source, float initialDelay);
    public OnTimerChange _onTimerStart;
    public OnTimerChange _onTimerEnd;



    void Start()
    {
        if (_autoStart)
            StartCountdown();
    }


    public int GetTimeLeftInSeconds()
    {
        return (int)_countdownValue;
    }
    public float GetTimeLeft()
    {
        return _countdownValue;
    }
    public float GetInitialTime() {
        return _delayOfTheCountDown;
    }



    private void StartCountdown()
    {
        RefreshTimer();
    }

    private void RefreshTimer()
    {

        _countdownValue = _delayOfTheCountDown;

        NotifyTimerStart();
    }

   

    void Update () {

        if (_countdownValue > 0f)
        {
            _countdownValue -= Time.deltaTime;
            if (_countdownValue < 0)
            {
                NotifyEndOfTheTimer();
                RefreshTimer();
            }
        }

    }
    private void NotifyTimerStart()
    {
        if (_onTimerStart != null)
            _onTimerStart(this, _delayOfTheCountDown);
        _onTimerStartEvent.Invoke();
    }

    private void NotifyEndOfTheTimer()
    {
        if (_onTimerEnd != null)
            _onTimerEnd(this, _delayOfTheCountDown);
        _onTimerEndEvent.Invoke();
    }
}
