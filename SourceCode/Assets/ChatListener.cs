﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ChatListener : MonoBehaviour {


    public PlayersStateManager _playerState;
    public Text _lastIRCMessage;
    public Text _lastGuestTry;

	// Use this for initialization
	void Start () {
        _playerState._onUserSendMessage += DisplayAsLastMessage;

        Refresh();
	}

    private void Refresh()
    {
        _lastIRCMessage.text = "";
        _lastGuestTry.text = "";
    }

    private void DisplayAsLastMessage(string userName, string message)
    {
        if (message[0] == '!')
        {
            _lastGuestTry.text = message.Substring(1);
        }
        else if (message[0] != '#')
        {
            _lastIRCMessage.text = message;
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
