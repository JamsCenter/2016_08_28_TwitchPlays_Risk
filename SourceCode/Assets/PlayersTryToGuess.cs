﻿using UnityEngine;
using System.Collections;
using System;

public class PlayersTryToGuess : MonoBehaviour {

    public PlayersScoreBoard _score;
    public PlayersStateManager _players;
    public CelebrityFinderGame _celebityToGuest;

    public int _scoreBuyWordFound = 5;
    public int _scoreBuyLetterFound = 2;
    public int _scoreBuyLetterLost = -1;




    void Start () {
        _players._onUserSendMessage += OnMessageReceived;
    }

    private void OnMessageReceived(string userName, string message)
    {
       

            int indexOfIsIt = message.IndexOf("Is it ",StringComparison.CurrentCultureIgnoreCase);
        int lenghtOfCmdStart = 6;
        if (indexOfIsIt <= -1) {
            indexOfIsIt = message.IndexOf("!", StringComparison.CurrentCultureIgnoreCase);
            lenghtOfCmdStart = 1;
        }


        if (indexOfIsIt == 0) {
            string nameProposed = message.Substring(lenghtOfCmdStart);
            nameProposed = nameProposed.Replace('?', ' ');
            nameProposed =  nameProposed.Trim();

            Debug.Log("Proposition:" + nameProposed);

            for (int i = 0; i < nameProposed.Length; i++)
            {
                if (!_celebityToGuest.IsLetterAlreadyFound(i))
                {
                    bool isLetterValideGuess = _celebityToGuest.IsTheLetterValide(nameProposed[i], i);
                    Debug.Log("Good letter :" + nameProposed[i] + " -> " + isLetterValideGuess);
                    if (isLetterValideGuess)
                    {
                        Debug.Log("One win:" + i);
                        _score.AddPointTo(userName, _scoreBuyLetterFound);
                        _celebityToGuest.SetTheLetterAsFounded(i);
                        if (_celebityToGuest.IsCelebrityFound())
                            _score.AddPointTo(userName, _scoreBuyWordFound);



                    }
                    else
                    {
                        Debug.Log("This letter is wrong:" + i);
                        _score.AddPointTo(userName, _scoreBuyLetterLost);
                        if (_score.GetPointOf(userName) < 0)
                            _score.SetPointTo(userName, 0);
                    }
                }
                else {
                    Debug.Log("This letter is wrong:" + i);
                    _score.AddPointTo(userName, _scoreBuyLetterLost);
                    if (_score.GetPointOf(userName) < 0)
                        _score.SetPointTo(userName, 0);
                }
            }
        }
    }
    
}
