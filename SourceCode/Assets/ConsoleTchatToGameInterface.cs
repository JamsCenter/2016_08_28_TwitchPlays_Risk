using UnityEngine;
using System.Collections;

public class ConsoleTchatToGameInterface : MonoBehaviour {

    public string _userName="DefaultUser";
    
    public bool _listenToKeyboard;
    public string _isWritingText;

    public delegate void OnMessageDetected(string userName, string message);
    OnMessageDetected _onMessageDetected;


    public bool _useDebugPrint=true;

    void Awake() {

        if(_useDebugPrint)
            _onMessageDetected+= (string name, string msg)=>
            {
                Debug.Log("MSG(" + name + "): " + msg);
            };
    }

    void Update () {

        if (Input.anyKeyDown)
        {
            _isWritingText = _isWritingText + Input.inputString;
        }

        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Return) && !_listenToKeyboard)
        {
            _isWritingText = "";
            _listenToKeyboard = true;

        }
        else if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Return) && _listenToKeyboard)
        {
            if(_onMessageDetected!=null) 
                _onMessageDetected(_userName, _isWritingText);
            _listenToKeyboard = false;
            _isWritingText = "";
        }

        
	
	}
}
