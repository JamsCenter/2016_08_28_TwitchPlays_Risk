﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UI_TerritoryZone : MonoBehaviour {

    public Text _numberOfDefendingUnit;
    public ImageColorSwitcher _backGroundTerritoryImage;
    public ImageColorSwitcher _territoryImage;
    public Transform _rootOfTheCountry;

    void Start() {
       // SetTerritoryTo(UnityEngine.Random.Range(1,5), new Color(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f)));
    }


    public void SetTerritoryTo(int unitOnTerritory, Color allianceColor) {

        Color colorLighter = allianceColor;
        colorLighter.a = 1f;
        _territoryImage.SetColorTo(allianceColor, UnityEngine.Random.Range(0.5f, 2f));

        colorLighter = allianceColor * 2f;
        colorLighter.a = 1f;
        _backGroundTerritoryImage.SetColorTo(colorLighter, 1f);

        _numberOfDefendingUnit.text = "" + unitOnTerritory;

    }

    internal Transform GetRootOfTheCountry()
    {

        if (_rootOfTheCountry!=null)
            return _rootOfTheCountry;
        return _numberOfDefendingUnit.transform;
    }

    internal void SetDisplayTextTo(string textToDisplay)
    {
        _numberOfDefendingUnit.text =  textToDisplay;
    }
}
