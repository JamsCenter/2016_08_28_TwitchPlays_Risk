using UnityEngine;
using System.Collections;

public class ArmyInZone : MonoBehaviour {

    public string _armyId;
    public int _unitOnTheTerritory;



    public void SetUnitCountTo(string armyId, int unitCount)
    {
        _armyId = armyId;
        SetUnitCountTo(unitCount);
    }
    public void SetUnitCountTo(int unitCount)
    {
        if (unitCount < 0) unitCount = 0;
        _unitOnTheTerritory = unitCount;
    }
    public string GetArmyName() {
        return _armyId;
    }
    public int GetNumberOfUnit() {
        return _unitOnTheTerritory;
    }

    public void AddUnit(int unitToAdd)
    {
        int currentNumber = GetNumberOfUnit();
        SetUnitCountTo(currentNumber + unitToAdd);

    }
    public void SubstractUnit(int unitToAdd)
    {
        AddUnit(-unitToAdd);
    }


}
