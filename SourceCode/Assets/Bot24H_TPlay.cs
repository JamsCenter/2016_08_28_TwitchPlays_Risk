using UnityEngine;
using System.Collections;
using System;

public class Bot24H_TPlay : MonoBehaviour {

    public PlayersStateManager       _playersMessageManager;
    public BotSendMessageControl     _messageSender;

	// Use this for initialization
	void Awake () {
        _playersMessageManager._onUserSendMessage += MessageReceived;
        
	}

    private void MessageReceived(string userName, string message)
    {
        if (string.IsNullOrEmpty(message))
            return;

        message = message.Trim();

        if (message.ToLower().Equals("&time")) {
            string respond = userName + ": The server current time is " + DigitalClock.GetCurrentTime(true, true, true, false).GetTimeAsString();
            _messageSender.Whisper(userName, respond, BotSendMessageControl.MessagePriority.Classic);
        }
        else if (message.ToLower().IndexOf("&improvement:")>=0)
        {
            int messsageStartIndex = 12;
            string respond = userName + ": Thanks for the improvement suggestion.";
            _messageSender.Whisper(userName, respond, BotSendMessageControl.MessagePriority.Classic);
            print("Improvement: " + message.Substring(messsageStartIndex));
        }
        else if (message.ToLower().IndexOf("&lag: ")>=0 && message.Length==14)
        {
            //IMPROVE: Should be a regex

            int messsageStartIndex = 5;
            string hoursPart = message.Substring(messsageStartIndex);
            string[] times = hoursPart.Split(':');
            if (times.Length == 3) { 
                int hours=0;
                int minutes=0;
                int seconds=0;
                int.TryParse(times[0], out hours);
                int.TryParse(times[1], out minutes);
                int.TryParse(times[2], out seconds);


                DigitalClock clock = DigitalClock.GetCurrentTime(true, true, true,false).SetHoursTo(hours).SetMinutesTo(minutes).SetSecondesTo(seconds);
                int lagInSeconds = 0;//clock.GetLagInSeconds();
                //string respond = userName + ": The lag between the video and you is of "+lagInSeconds;
                string respond = userName + ": " + clock.GetTimeAsString()+"-> ?? <-"+DigitalClock.GetCurrentTime(true,true, true, false).GetTimeAsString();
                _messageSender.Whisper(userName, respond, BotSendMessageControl.MessagePriority.Classic);
                print("Lag of: " + lagInSeconds);
            }
        }
    }
    
}
