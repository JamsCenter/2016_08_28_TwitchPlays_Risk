﻿using UnityEngine;
using System.Collections;
using System;

public class LinkTimeToPanelDigitalClock : MonoBehaviour {

    public PanelDigitalClock _digitalClockPanel;
	void Start () {
        InvokeRepeating("RefreshClock", 0f, 1f);
	}
	
	void RefreshClock()
    {
        
        _digitalClockPanel.GetDigitalClockData().SetHoursTo(DateTime.Now.Hour).SetMinutesTo(DateTime.Now.Minute).SetSecondesTo(DateTime.Now.Second);
        _digitalClockPanel.Refresh();
        

    }
}
