﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class HangManString
{
    public HangManString(string value)
    { _value = value; }

    [SerializeField]
    [Tooltip("Name that is hidden")]
    private string _value;

    public int Length { get { return _value.Length; }  }

    public string GetValue()
    {
        return _value;
    }
    public string GetHiddenStringFormat(bool[] whatToShow, char letterToHide = '_')
    {
        char[] hiddenString = new char[_value.Length];

        //print long hide line of the word lengh
        for (int i = 0; i < _value.Length; i++)
        {
            hiddenString[i] = letterToHide;
        }
        //Print white space space
        int lastIndexOfSpace = _value.IndexOf(' ', 0);
        while (lastIndexOfSpace >= 0)
        {
            hiddenString[lastIndexOfSpace] = ' ';
            if (lastIndexOfSpace + 1 >= _value.Length)
                break;
            lastIndexOfSpace = _value.IndexOf(' ', lastIndexOfSpace+1);
        }

        for (int i = 0; i < whatToShow.Length; i++)
        {
            if (i < _value.Length && whatToShow[i] == true)
            {
                hiddenString[i] = _value[i];

            }
        }

        return new string(hiddenString);
    }

    public bool IsItTheGoodLetter(char letter, int position)
    {
        return position < _value.Length && position >= 0 &&  (""+_value[position]).ToLower() == ("" + letter).ToLower() ;
    }




}