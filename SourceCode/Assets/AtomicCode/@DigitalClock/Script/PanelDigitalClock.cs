﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PanelDigitalClock : MonoBehaviour {

    public Text _textClock;

    [SerializeField]
    private DigitalClock _digitalClock;

    public void OnValidate()
    {
        Refresh();
    }

   public  DigitalClock  GetDigitalClockData()
    {
        return _digitalClock;
    }

    internal void SetTimeTo(int secondleft)
    {
        _digitalClock.SetTimerWithSeconds(secondleft);
        SetTextWith(_digitalClock);
    }

    private void SetTextWith(DigitalClock digitalClock)
    {
        if (!_textClock) return;
        _textClock.text = digitalClock.GetTimeAsString();
       
    }

    public void Refresh()
    {
        SetTextWith(_digitalClock);
    }
}
