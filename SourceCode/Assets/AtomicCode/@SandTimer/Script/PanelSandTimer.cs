﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PanelSandTimer : SandTimer
{


    [SerializeField]
    private Image _topTimeSand;

    [SerializeField]
    private Image _botTimeSand;


    public override void FillBotPartOfSandTimer(float pourcent)
    {
        if (_botTimeSand)
            _botTimeSand.fillAmount = pourcent;
    }

    public override void FillTopPartOfSandTimer(float pourcent)
    {
        if (_topTimeSand)
            _topTimeSand.fillAmount = pourcent;
    }
}
