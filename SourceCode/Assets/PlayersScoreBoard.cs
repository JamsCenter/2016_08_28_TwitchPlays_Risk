﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;
using System;

public class PlayersScoreBoard : MonoBehaviour {


    public Dictionary<string, int> _playersScore = new Dictionary<string, int>();
    [Header("Events")]
    public UnityEvent _onValueChange;

   

    public int GetPointOf(string playerId) {

        if (!_playersScore.ContainsKey(playerId))
            return 0;
        else return _playersScore[playerId];

    }

    public void AddPointTo(string playerId, int score) {

        CheckAndAddPlayer(playerId);
        _playersScore[playerId] += score;
        NotifyChange();

    }

    private void NotifyChange()
    {
        _onValueChange.Invoke();
    }

    public void SetPointTo(string playerId, int score) {
        CheckAndAddPlayer(playerId);
        _playersScore[playerId] = score;
        NotifyChange();
    }

    void CheckAndAddPlayer(string playerId) {
        if (!_playersScore.ContainsKey(playerId))
            _playersScore.Add(playerId, 0);
    }

    public List<string> GetPlayersIdName()
    {
        return new List<string>(this._playersScore.Keys);
    }

    internal void ResetDate()
    {
        _playersScore.Clear();
        NotifyChange();
    }

    public List<string> GetPlayersIdNameSortByScore(int numberOfPlayer)
    {
        List<string> topResult = new List<string>();
        int scoreCount = 0;
        foreach (var item in _playersScore.OrderByDescending(i => i.Value))
        {
            topResult.Add( item.Key );

            scoreCount++;
            if (scoreCount >= numberOfPlayer)
                break;
        }
        return topResult;
    }






}
